package dmcompanion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CharacterOverviewPanel extends JPanel {

  private Character character;

  private String display_name;
  private String display_level;

  private final int padding = 20;

  public CharacterOverviewPanel() {
    character = new Character();

    init();
  }

  public CharacterOverviewPanel(Character c) {
    character = c;

    init();
  }

  private void init() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    setBorder(BorderFactory.createEmptyBorder(padding, padding, padding, padding));

    display_name = "<html><center>" + Utilities.capitalize(character.name()) + "</center></html>";
    display_level = "<html><center>Level: " + character.level() + "</html></center>";

    JLabel character_name_label = new JLabel(display_name, SwingConstants.CENTER);
    add(character_name_label);

    JLabel character_level_label = new JLabel(display_level, SwingConstants.CENTER);
    character_level_label.setBorder(BorderFactory.createEmptyBorder(0,0,20,0));
    add(character_level_label);

    JButton view_full_character_button = new JButton("View Full Character");
    view_full_character_button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        // System.out.println("Pushed btn");
        JFrame fullCharacter = new JFrame();
        fullCharacter.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        fullCharacter.getContentPane().add(new CharacterPanel(character));
        fullCharacter.pack();
        fullCharacter.setVisible(true);
      }
    } );
    add(view_full_character_button);

    JButton edit_character_button = new JButton("Edit Character");
    edit_character_button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        JFrame editCharacter = new JFrame();
        editCharacter.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        editCharacter.pack();
        editCharacter.setVisible(true);
        System.out.println("Editing character...");
      }
    });
    add(edit_character_button);
  }

}
