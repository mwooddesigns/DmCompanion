package dmcompanion;

import java.io.*;
import java.util.HashMap;

public class CharacterStorage {

  private static HashMap<Object, Object> loadCharacterFromFile(String filename) {

    HashMap<Object, Object> raw_character = new HashMap<Object, Object>();
    String line = null;

    try {

      FileReader fileReader = new FileReader(filename);

      BufferedReader bufferedReader = new BufferedReader(fileReader);

      while((line = bufferedReader.readLine()) != null) {
        String[] lineArray = line.split(":");
        raw_character.put(lineArray[0].toLowerCase().trim(), lineArray[1].toLowerCase().trim());
      }

      bufferedReader.close();

    } catch(FileNotFoundException e) {
      System.out.println("Unable to open file '" + filename + "'");
    } catch(IOException e) {
      System.out.println("Error reading file '" + filename + "'");
      // Or we could just do this:
      e.printStackTrace();
    }

    return raw_character;

  }

  private static HashMap<String, Integer> mapStats(String stats_string) {
    HashMap<String, Integer> mapped_stats = new HashMap<String, Integer>();
    String[] stats_array = stats_string.split(";");
    for(int i = 0; i < stats_array.length; i++) {
      String[] stat = stats_array[i].split(",");
      mapped_stats.put(stat[0], Integer.parseInt(stat[1]));
    }
    return mapped_stats;
  }

  private static HashMap<String, String> mapDiceRolls(String dice_rolls_string) {
    HashMap<String, String> mapped_dice_rolls = new HashMap<String, String>();
    String[] dice_roll_array = dice_rolls_string.split(";");
    for(int i = 0; i < dice_roll_array.length; i++) {
      String[] dice_roll = dice_roll_array[i].split(",");
      mapped_dice_rolls.put(dice_roll[0], dice_roll[1]);
    }
    return mapped_dice_rolls;
  }

  public static Character loadCharacter(String filename) {
    Character loaded_character = new Character();
    HashMap<Object, Object> character_data = loadCharacterFromFile(filename);
    // System.out.println(character_data.toString());
    // System.out.println(character_data.get("name"));
    loaded_character.name((String) character_data.get("name"));
    loaded_character.level(Integer.parseInt((String) character_data.get("level")));
    loaded_character.age(Integer.parseInt((String) character_data.get("age")));
    loaded_character.race((String) character_data.get("race"));
    loaded_character.stats(mapStats((String) character_data.get("stats")));
    loaded_character.dice_rolls(mapDiceRolls((String) character_data.get("dice_rolls")));
    loaded_character.bio((String) character_data.get("bio"));
    return loaded_character;
  }

}
