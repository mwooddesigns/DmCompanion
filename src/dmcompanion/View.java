package dmcompanion;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame {

    private DmCompanion app_data = new DmCompanion();
    private Character[] party = app_data.characters();

    View(DmCompanion application) {

      setTitle("DM Companion");

      app_data = application;

      // System.out.println(party.length);

      JPanel mainPanel = new JPanel();

      for(int i = 0; i < party.length; i++) {
        CharacterOverviewPanel toAdd = new CharacterOverviewPanel(party[i]);
        mainPanel.add(toAdd);
      }

      // for(int i = 0; i < party.length; i++) {
      //   CharacterPanel toAdd = new CharacterPanel(party[i]);
      //   mainPanel.add(toAdd);
      // }

      // JFrame frame = new JFrame("DmCompanion");
      // CharacterPanel testPanel = new CharacterPanel(party[0]);
      // getContentPane().add(testPanel);

      // showCharacters();

      getContentPane().add(mainPanel);
      pack();
      // setSize(new Dimension(500, 500));
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setVisible(true);
    }

    public void showCharacters() {
      getContentPane().removeAll();
      JPanel charactersDisplay = new JPanel(new BoxLayout(this, BoxLayout.X_AXIS));
      for(int i = 0; i < app_data.characters().length; i++) {
        CharacterPanel adventurerPanel = new CharacterPanel(party[i]);
        getContentPane().add(adventurerPanel);
      }
    }

}
