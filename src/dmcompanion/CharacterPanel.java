package dmcompanion;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class CharacterPanel extends JPanel {

  private Character character;

  private String display_name = "Unknown";
  private String display_level = "Level: 1";
  private String display_age = "Age: ???";
  private String display_race = "Race: ???";
  private String display_stats = "";
  private String display_dice_rolls = "";
  private String display_bio = "";

  private final int padding = 20;

  CharacterPanel() {
    character = new Character();

    init();
  }

  CharacterPanel(Character c) {
    character = c;

    init();
  }

  private void init() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    setBorder(BorderFactory.createEmptyBorder(padding, padding, padding, padding));

    display_name = "<html><center>" + Utilities.capitalize(character.name()) + "</center></html>";
    display_level = "<html><center>Level: " + character.level() + "</center></html>";
    display_age = "<html><center>Age: " + character.age() + "</center></html>";
    display_race = "<html><center>Race: " + Utilities.capitalize(character.race()) + "</center></html>";
    HashMap<String, Integer> raw_stats = character.stats();
    display_stats += "<html><center>";
    for(String currentKey : raw_stats.keySet()) {
      display_stats += Utilities.capitalize(currentKey) + ": " + raw_stats.get(currentKey) + "<br>";
    }
    display_stats += "</center></html>";

    HashMap<String, String> raw_dice_rolls = character.dice_rolls();
    display_dice_rolls += "<html><center>";
    for(String currentKey : raw_dice_rolls.keySet()) {
      display_dice_rolls += Utilities.capitalize(currentKey) + ": " + raw_dice_rolls.get(currentKey) + "<br>";
    }
    display_dice_rolls += "</center></html>";

    display_bio = "<html><center style='width: 250px'>" + Utilities.capitalize(character.bio()) + "</center></html>";

    JLabel character_name_label = new JLabel(display_name, SwingConstants.CENTER);
    add(character_name_label);

    JLabel character_level_label = new JLabel(display_level, SwingConstants.CENTER);
    add(character_level_label);

    JLabel character_age_label = new JLabel(display_age, SwingConstants.CENTER);
    add(character_age_label);

    JLabel character_race_label = new JLabel(display_race, SwingConstants.CENTER);
    add(character_race_label);

    JLabel character_stats_label = new JLabel(display_stats, SwingConstants.CENTER);
    add(character_stats_label);

    JLabel character_dice_rolls_label = new JLabel(display_dice_rolls, SwingConstants.CENTER);
    add(character_dice_rolls_label);

    JLabel character_bio_label = new JLabel(display_bio, SwingConstants.CENTER);
    add(character_bio_label);
  }

}
