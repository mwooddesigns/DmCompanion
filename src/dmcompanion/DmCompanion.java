package dmcompanion;

import java.io.File;

public class DmCompanion {
  private static Character[] all_characters;

  DmCompanion() {
    loadCharacters();
  }

  public static void loadCharacters() {

    File character_folder= new File("./characters");
    File[] list_of_character_files = character_folder.listFiles();

    all_characters = new Character[list_of_character_files.length];

    for (int i = 0; i < list_of_character_files.length; i++) {
      if (list_of_character_files[i].isFile()) {
        all_characters[i] = CharacterStorage.loadCharacter("./characters/" + list_of_character_files[i].getName());
        // System.out.println(all_characters[i].stats().get("strength"));
        // System.out.println(all_characters[i].toString());
      }
    }
  }

  public Character[] characters(Character[] new_characters) {
    all_characters = new_characters;
    return all_characters;
  }

  public Character[] characters() {
    return all_characters;
  }
}
