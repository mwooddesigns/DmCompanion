package dmcompanion;

import java.util.HashMap;

public class Character {

  private String character_name;
  private int character_level;
  private int character_age;
  private HashMap<String, Integer> character_stats;
  private HashMap<String, String> character_dice_rolls;
  private String character_bio;
  private String character_race;

  public Character() {
    character_name = "Unknown";
    character_level = 1;
    character_age = -1;
    character_race = "human";
    character_stats = new HashMap<String, Integer>();
    character_dice_rolls = new HashMap<String, String>();
    character_bio = "Origin Unknown";
  }

  public Character(String name,int level, int age, String race, HashMap<String, Integer> stats, HashMap<String, String> dice_rolls, String bio) {
    character_name = name;
    character_level = level;
    character_age = age;
    character_race = race;
    character_stats = stats;
    character_dice_rolls = dice_rolls;
    character_bio = bio;
  }

  public String name(String new_name) {
    character_name = new_name;
    return character_name;
  }

  public String name() {
    return character_name;
  }

  public int level(int new_level) {
    character_level = new_level;
    return character_level;
  }

  public int level() {
    return character_level;
  }

  public int age(int new_age) {
    character_age = new_age;
    return character_age;
  }

  public int age() {
    return character_age;
  }

  public String race(String new_race) {
    character_race = new_race;
    return character_race;
  }

  public String race() {
    return character_race;
  }

  public HashMap<String, Integer> stats(HashMap<String, Integer> new_stats) {
    character_stats = new_stats;
    return character_stats;
  }

  public HashMap<String, Integer> stats() {
    return character_stats;
  }

  public HashMap<String, String> dice_rolls(HashMap<String, String> new_dice_rolls) {
    character_dice_rolls = new_dice_rolls;
    return character_dice_rolls;
  }

  public HashMap<String, String> dice_rolls() {
    return character_dice_rolls;
  }

  public String bio(String new_bio) {
    character_bio = new_bio;
    return character_bio;
  }

  public String bio() {
    return character_bio;
  }

  public String toString() {
    return "name: " + character_name + "\r\nage: " + character_age + "\r\n" + character_stats.toString();
  }

}
